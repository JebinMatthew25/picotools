function submitForm() {
    const formHtml = $("#rightContent").val();
    const toSubmit = $("#toSubmit");
    for (let i = $("#from").val(); i <= $("#till").val(); i++) {
        const finalForm = formHtml.replace(new RegExp($("#what").val(), 'g'), i);
        toSubmit.html(finalForm);
        $("#myForm").submit();
    }
    toSubmit.html("");
}

function genForm() {
    let url = $("#url-bar").val();
    let method =$("#method").val();
    url = url.includes(": ") ? url.split(': ')[1] : url;
    let leftContent = $("#leftContent").val();
    let rightContent = '';
    rightContent += `<body onload="document.forms['myForm'].submit()">\n`;
    rightContent += `    <form id="myForm" method="${method}" action="${url}" target="_blank">\n`;
    let name, value;
    leftContent.split('\n').forEach(function (line) {
            [name, value] = line.split(': ');
            rightContent += `        <input name="${name}" value='${value}'>\n`;
        }
    );
    rightContent += `    </form>\n`;
    rightContent += `</body>\n`;
    $("#rightContent").val(rightContent);
}
