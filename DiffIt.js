function tokenize(content, delimiter) {
    const replaced = content.replace(/[\r\n]/g, '');
    return replaced.split(delimiter || ' ').map(s => (s).trim());
}

function tsv() {
    let text = $("#TSV").val();
    text = text.replace(/[\r\n]/g, '');
    const textArr = text.split('\t');
    $("#leftContent").val(textArr[1]);
    $("#rightContent").val(textArr[3]);
}

function addToDOM(symbol, content) {
    const html =
        `<tr>
            <th scope="row">${symbol}</th>
            <td>${content}</td>
        </tr>`;
    $("#list").append(html);
}

function diffIt() {
    const diffPane = $("#diffPane");
    diffPane.hide();
    $("#list").html("");
    const delimiter = $("#delimit").val();
    const left = tokenize($("#leftContent").val(), delimiter);
    const right = tokenize($("#rightContent").val(), delimiter);

    let removed = left.filter(x => right.indexOf(x) < 0);
    let added = right.filter(x => left.indexOf(x) < 0);
    let merged = removed.map(x => ['-', x]).concat(added.map(x => ['+', x]));

    merged.sort(function (a, b) {
        if (a[1] === b[1]) {
            return 0;
        } else {
            return (a[1] < b[1]) ? -1 : 1;
        }
    });

    merged.forEach(x => addToDOM(x[0], x[1]))
    diffPane.show();
}